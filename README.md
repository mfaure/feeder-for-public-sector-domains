# feeder for public sector domains

*Feeder for public sector domains* is an online form to easily add domains to the repository
[Noms de domaines organismes secteur public](https://gitlab.adullact.net/dinum/noms-de-domaine-organismes-secteur-public/-/tree/master]

## Visuals

*Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see
GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.*

## Installation

(to be done)

## Usage

(to be done)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors and acknowledgment

Matthieu Faure, ADULLACT

## License

[AGPL 3.0 or later](https://choosealicense.com/licenses/agpl-3.0/)

## Project status

Under development
